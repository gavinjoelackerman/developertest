﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using DevloperTest.Models;
using Newtonsoft.Json;

namespace DevloperTest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MenuItemsController : ApiController
    {
        private MenuItemDataModel db = new MenuItemDataModel();

        // GET: api/MenuItems
        public IQueryable<MenuItem> GetMenuItems()
        {
            return db.MenuItems;
        }

      
        [ResponseType(typeof(MenuItem))]
        public string GetMenuItem(int id)
        {
            string menuItemJson = "";
            MenuItem menuItem = db.MenuItems.Find(id);
           
            if (menuItem == null)
            {
                return menuItemJson;
            }
            List<MenuItem> menuItemList = db.MenuItems.Where(x => x.ParentId == menuItem.Id).ToList();
            List<MenuItem> newItemList = new List<MenuItem>();
            newItemList.Add(menuItem);
            if (menuItemList.Count > 0)
            {
                foreach (var menuListItem in menuItemList)
                {
                    newItemList.Add(menuListItem);
                    getSubItems(menuListItem, newItemList);
                }
            }
                  
            menuItemJson = JsonConvert.SerializeObject(newItemList);
            return menuItemJson;
 
        }
     
        public void getSubItems(MenuItem menuItem, List<MenuItem> newItemList)
        {

       
            List<MenuItem> subMenuItemList = db.MenuItems.Where(x => x.ParentId == menuItem.Id).ToList();
            if (subMenuItemList.Count > 0)
            { 
                    foreach (var menuListItem in subMenuItemList.ToList())
                    {
                        newItemList.Add(menuListItem);
                        getSubItems(menuListItem,newItemList);
              
                    }
             }
         
        }
        [ResponseType(typeof(MenuItem))]
        public string GetMenuItem(string value)
        {
            string menuItemJson = "";
            MenuItem menuItem = db.MenuItems.Where(x => x.Title.ToLower() == value.ToLower()).FirstOrDefault();
            if (menuItem == null)
            {
                return menuItemJson;
            }
            List<MenuItem> menuItemList = db.MenuItems.Where(x => x.ParentId == menuItem.Id).ToList();
            List<MenuItem> newItemList = new List<MenuItem>();
            newItemList.Add(menuItem);
            if (menuItemList.Count > 0)
            {
                foreach (var menuListItem in menuItemList)
                {
                    newItemList.Add(menuListItem);
                    getSubItems(menuListItem, newItemList);
                }
            }


            menuItemJson = JsonConvert.SerializeObject(newItemList);
            return menuItemJson;
     
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MenuItemExists(int id)
        {
            return db.MenuItems.Count(e => e.Id == id) > 0;
        }
    }
}